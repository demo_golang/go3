package main

import (
	"go3/controller"
	"go3/databases"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db       *gorm.DB                  = databases.OpenDatabase()
	authCtrl controller.AuthController = controller.NewAuthController()
)

func main() {

	defer databases.CloseDatabase(db)

	r := gin.Default()

	autRouter := r.Group("/auth")
	{
		autRouter.POST("/login", authCtrl.Login)
		autRouter.POST("/register", authCtrl.Register)
	}

	r.Run(":8888")
}
