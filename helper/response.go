package helper

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"messenger"`
	Error   interface{} `json:"error"`
	Data    interface{} `json:"data"`
}

type EmptyObjectResponse struct{}

func BuildResponse(status int, message string, data interface{}) Response {
	return Response{
		Status:  status,
		Message: message,
		Error:   nil,
		Data:    data,
	}
}

func BuildErrorResponse(status int, message string, err interface{}, data interface{}) Response {
	return Response{
		Status:  status,
		Error:   err,
		Message: message,
		Data:    data,
	}
}
