package dto

import "time"

type UserList struct {
	Id       string    `json:"id"`
	UserName string    `json:"userName"`
	Password int       `json:"password"`
	FullName string    `json:"fullName"`
	DOB      time.Time `json:"dob"`
	Gender   bool      `json:"gender"`
	IsAdmin  bool
}
