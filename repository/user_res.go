package response

import (
	"go3/entity"
	"log"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	GetListUser() []entity.DataUser
}

type userConnection struct {
	connection *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userConnection{connection: db}
}

func (db *userConnection) GetListUser() []entity.DataUser {
	data_users := []entity.DataUser{}
	db.connection.Find(&data_users)
	return data_users
}

func hashAndSalt(pass []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pass, bcrypt.MinCost)

	if err != nil {
		log.Println(err)
		panic(err)
	}

	return string(hash)
}
