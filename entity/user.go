package entity

import (
	"time"
)

type DataUser struct {
	Id       string    `gorm:"column:id;primaryKey" json:"id"`
	UserName string    `gorm:"column:user_name" json:"userName"`
	Password int       `gorm:"column:pass_word" json:"password"`
	FullName string    `gorm:"column:full_name" json:"fullName"`
	DOB      time.Time `gorm:"column:dob" json:"dob"`
	Gender   bool      `gorm:"column:gender" json:"gender"`
	IsAdmin  bool      `gorm:"column:is_admin" json:"isAdmin"`
}
