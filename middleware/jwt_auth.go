package middleware

import (
	"go3/helper"
	"go3/service"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

func AuthorizeJWT(jwtserv service.JWTService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			response := helper.BuildErrorResponse(http.StatusBadRequest, "không tìm thây token", nil, nil)
			c.Copy().AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}

		token, err := jwtserv.ValidateToken(authHeader)
		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			log.Println("Claim[userid]: ", claims["userid"])
		} else {
			response := helper.BuildErrorResponse(http.StatusUnauthorized, "Token is not valid", err.Error(), nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		}
	}
}
