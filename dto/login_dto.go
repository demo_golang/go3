package dto

type LoginParams struct {
	UserName string `json:"userName"`
	Password int    `json:"password"`
}
